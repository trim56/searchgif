//
//  WebViewController.swift
//  searchGif
//
//  Created by Павел Зорин on 12.03.2020.
//  Copyright © 2020 Павел Зорин. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKUIDelegate {

    @IBOutlet weak var blockView: UIView!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var preload: UIActivityIndicatorView!
    // Ссылка на ресурс
    var link: String = ""
    // Текст заголовка навигационного бара
    var navBarTitle: String = ""
    var webView: WKWebView!
    
    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self
        webView.uiDelegate = self
        blockView.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            webView.leadingAnchor.constraint(equalTo: blockView.leadingAnchor),
            webView.topAnchor.constraint(equalTo: blockView.topAnchor),
            webView.trailingAnchor.constraint(equalTo: blockView.trailingAnchor),
            webView.bottomAnchor.constraint(equalTo: blockView.bottomAnchor)
        ])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBar.topItem?.title = navBarTitle
        let myURL = URL(string: link)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
    // Метод установки стиля StatusBar
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}

extension WebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        preload.stopAnimating()
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        preload.startAnimating()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        preload.stopAnimating()
    }
}
