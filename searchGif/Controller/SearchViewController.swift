//
//  ViewController.swift
//  searchGif
//
//  Created by Павел Зорин on 11.03.2020.
//  Copyright © 2020 Павел Зорин. All rights reserved.
//

import UIKit
import Alamofire

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchField: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    // ID ячейки таблицы
    private let cellId = "gifCell"
    // Массив данных получаемых с сервера
    private var gifData: [SearchModelData]?
    // Текст поиска по умолчанию
    let defaultSearchText: String = "Troll"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Настройка стиля поля Поиска
        let textFieldInsideSearchBar = searchField.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = .white
        searchField.layer.borderColor = UIColor.systemGray.cgColor
        searchField.layer.borderWidth = 1
        searchField.delegate = self
        // Регистрируем параметры таблицы
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        self.connectAF(searchString: defaultSearchText)
    }
    // Запрос данных
    private func connectAF(searchString: String = "") {
        // Пускаем спиенер
        searchField.isLoading = true
        let params: [String: Any] = [
            "api_key": "X9FhhMkEbiT8pXbKQ9EZWxKU0bSyYHnk",
            "q": searchString,
            "lang": "ru"
        ]
        AF.request(
            "https://api.giphy.com/v1/gifs/search",
            method: .get,
            parameters: params
        )
            .validate()
            .responseDecodable(of: SearchModel.self) { response in
                switch response.result {
                case .success:
                    // Если результат не удалось преобразовать
                    guard let anyData = response.value
                        else {
                            self.dialogAlert(message: "Ошибка получения данных.")
                            return
                    }
                    self.gifData = anyData.data
                    self.tableView.reloadData()
                case let .failure(error):
                    // Если это ошибка сериализации данных
                    if error.isResponseSerializationError {
                        
                    }else {
                        guard let responseDescr = error.errorDescription
                            else {
                                self.dialogAlert(message: "Ошибка не определена.")
                                return
                        }
                        self.dialogAlert(message: responseDescr)
                    }
                }
                // Останавливаем спинер
                self.searchField.isLoading = false
        }
    }
    // Диалоговое окно
    private func dialogAlert(message: String) {
        let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    // Срабатывает при нажатии на кнопку Done на клавиатуре
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchField.resignFirstResponder()
    }
    // Метод установки стиля StatusBar
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    // Возврат в текущее представление из предыдущего
    @IBAction func unwindToViewControllerSearchVC (sender: UIStoryboardSegue){}
}
// MARK: Table DataSource
extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = gifData else {
            return 0
        }
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Запрашиваем повторно используемую ячейку, что бы разместить в ней данные
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        // Если нет повторноиспользуемой ячейки, тогда создаем новую
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellId)
        }
        // Присваеваем параметры ячейке
        cell?.textLabel?.text = gifData?[indexPath.row].title
        return cell!
    }
}
// MARK: Table Delegate
extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let mvc = storyboard.instantiateViewController(withIdentifier: "web") as? WebViewController {
            mvc.link = gifData?[indexPath.row].embedUrl ?? ""
            mvc.navBarTitle = selectedCell?.textLabel?.text ?? ""
            self.present(mvc, animated: true, completion: nil)
        }
    }
}
// MARK: SearchBar
extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Если строка поиска не пустая
        if searchText != "" {
            self.connectAF(searchString: searchText)
        }else {
            self.connectAF(searchString: defaultSearchText)
        }
    }
}

