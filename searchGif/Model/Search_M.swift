//
//  Search_M.swift
//  searchGif
//
//  Created by Павел Зорин on 12.03.2020.
//  Copyright © 2020 Павел Зорин. All rights reserved.
//

struct SearchModel: Decodable {
    let data: [SearchModelData]
}

struct SearchModelData: Decodable {
  var title: String
  var embedUrl: String
  
  enum CodingKeys: String, CodingKey {
      case title
      case embedUrl = "embed_url"
  }
  
  init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      self.title = try container.decode(String.self, forKey: .title)
      self.embedUrl = try container.decode(String.self, forKey: .embedUrl)
  }
}

